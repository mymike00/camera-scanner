[![pipeline status](https://gitlab.com/jonnius/camera-scanner/badges/master/pipeline.svg)](https://gitlab.com/jonnius/camera-scanner/commits/master)

<img width="200px" src="assets/logo.png" />

# Camera Scanner - An Ubuntu Touch Document Scanner App

This is an Ubuntu Touch App to scan documents using your camera.

## How to get it

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/camerascanner.jonnius)

Or build it yourself following instructions below.

## Building the app

### Dependencies
Install [clickable](https://clickable-ut.dev/en/latest/), which is used to build this app and dependencies.

This app depends on OpenCV. You can easily build OpenCV by running

    git submodule update --init --recursive
    clickable build --libs --arch arm64 # or armhf, depending on your device
    clickable build --libs --arch amd64 # If you want to do desktop builds, too

This may take quite some time, but only needs to be done once.

### Installation
To build and launch the app, simply run

    clickable chain build install launch logs --arch arm64 # or armhf
    clickable desktop # to try a desktop build

See [clickable documentation](https://clickable-ut.dev/en/latest/) for details.

## Code Style/Formatting
### C++
Clang-format is used to keep the code style consistent and should be run before
committing any changes to C++ code.

Install clang-format

    sudo apt install clang-format

Then you can either run clang-format as a git pre commit hook (preffered) or run
the apply-format script to format any staged changes.

    ./apply-format -i plugins/ImageProcessing/*.{h,cpp}

### QML
Qmlfmt is used to keep the QML code style clean and should be run before
committing any changes to QML code.

Install [Qmlfmt](https://github.com/jesperhh/qmlfmt#build-instructions). You can
either run qmlfmt as a git pre commit hook (preffered) or run
`qmlfmt -w <file>` on any qml file.

## Contributors
* Jonatan Hatakeyama Zeidler (jonnius)
* Joan CiberSheep (cibersheep)
* Stefan Weng (stefwe)
* Christian Pauly (krille)
* Anne017

The image processing algorithm is based on the one used in [OpenNoteScanner](https://github.com/ctodobom/OpenNoteScanner).
