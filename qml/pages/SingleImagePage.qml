import QtQuick 2.9
import Ubuntu.Components 1.3
import ImageProcessing 1.0
import Ubuntu.Content 1.3
import Ubuntu.Components.Popups 1.3
import "../components"

Page {
    id: singleImagePage
    width: parent.width

    property string currentImage
    property int currentIndex
    property string currentID
    property bool pageLoaded: false

    header: SingleImageHeader {
        id: singleImageHeader
    }

    Flickable {
        id: flickable
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
            bottom: detailsItem.top
            topMargin: singleImagePage.header.height
        }

        contentHeight: height
        contentWidth: width

        Item {
            height: flickable.height
            width: flickable.width

            UbuntuShape {
                id: image
                aspect: UbuntuShape.Flat

                anchors {
                    fill: parent
                    leftMargin: gridmargin
                    rightMargin: gridmargin
                }

                source: Image {
                    sourceSize.width: image.width
                    sourceSize.height: image.height
                    source: currentImage
                    cache: false
                }
                sourceFillMode: UbuntuShape.PreserveAspectFit
            }
        }
    }

    MouseArea {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: detailsItem.height
        property bool ignoring: false
        onPressed: {
            ignoring = false
            if (detailsItem.showProgress == 0
                    && mouseY < height - units.gu(2)) {
                print("rejecting mouse")
                mouse.accepted = false
                ignoring = true
            }
        }
        onMouseYChanged: {
            if (ignoring) {
                return
            }

            detailsItem.showProgress = (height - mouseY) / height
        }
        onReleased: {
            if (detailsItem.showProgress > .5) {
                detailsItem.showProgress = 1
            } else {
                detailsItem.showProgress = 0
            }
        }
    }

    Item {
        id: detailsItem
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: singleImagePage.height / 2
        anchors.bottomMargin: Math.min(Math.max(
                                           -height + units.gu(2),
                                           -height + showProgress * height), 0)
        property real showProgress: 0

        Behavior on anchors.bottomMargin {
            UbuntuNumberAnimation {}
        }

        Rectangle {
            id: dragHandle
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            height: units.gu(2)
            color: bgColor

            Row {
                anchors.centerIn: parent
                spacing: units.gu(1)
                Repeater {
                    model: 3
                    Rectangle {
                        height: units.gu(1)
                        width: height
                        radius: height / 2
                        color: Qt.lighter(Qt.lighter(bgColor))
                    }
                }
            }
        }

        Column {
            width: parent.width
            spacing: units.gu(2)

            anchors.top: dragHandle.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            ListItemLayout {
                id: filterModeSwitch
                title.text: i18n.tr("Filter Mode")
                title.color: theme.palette.normal.baseText

                Switch {
                    id: filterSwitch
                    checked: ImageProcessing.filterMode
                    onCheckedChanged: if (pageLoaded)
                                          reprocessImage()
                }
            }

            ListItemLayout {
                id: colorModeSwitch
                visible: filterSwitch.checked
                title.text: i18n.tr("Color Mode")
                title.color: theme.palette.normal.baseText

                Switch {
                    id: colorSwitch
                    checked: ImageProcessing.colorMode
                    onCheckedChanged: if (pageLoaded)
                                          reprocessImage()
                }
            }

            Row {
                id: thrRow
                anchors.horizontalCenter: parent.horizontalCenter
                visible: filterSwitch.checked && colorSwitch.checked
                spacing: units.gu(2)

                property var sliderWidth: parent.width - thrLabel.width - units.gu(
                                              6)

                Label {
                    id: thrLabel
                    width: text.width
                    wrapMode: Text.Wrap
                    text: i18n.tr("Color Thr")
                }

                Slider {
                    id: colorThrSlider
                    live: false
                    function formatValue(v) {
                        return v.toFixed(2)
                    }
                    minimumValue: 0.25
                    maximumValue: 1.0
                    value: ImageProcessing.colorThr

                    width: thrRow.sliderWidth
                    height: units.gu(2)

                    onPressedChanged: {
                        if (!pressed) {
                            reprocessImage()
                        }
                    }
                }
            }

            Row {
                id: gainRow
                anchors.horizontalCenter: parent.horizontalCenter
                visible: filterSwitch.checked && colorSwitch.checked
                spacing: units.gu(2)

                property var sliderWidth: parent.width - gainLabel.width - units.gu(
                                              6)

                Label {
                    id: gainLabel
                    width: text.width
                    wrapMode: Text.Wrap
                    text: i18n.tr("Contrast")
                }

                Slider {
                    id: colorGainSlider
                    live: false
                    function formatValue(v) {
                        return v.toFixed(2)
                    }
                    minimumValue: 1.00
                    maximumValue: 2.00
                    value: ImageProcessing.colorGain

                    width: gainRow.sliderWidth
                    height: units.gu(2)

                    onPressedChanged: {
                        if (!pressed) {
                            reprocessImage()
                        }
                    }
                }
            }

            Row {
                id: biasRow
                anchors.horizontalCenter: parent.horizontalCenter
                visible: filterSwitch.checked && colorSwitch.checked
                spacing: units.gu(2)

                property var sliderWidth: parent.width - biasLabel.width - units.gu(
                                              6)

                Label {
                    id: biasLabel
                    width: text.width
                    wrapMode: Text.Wrap
                    text: i18n.tr("Brightness")
                }

                Slider {
                    id: colorBiasSlider
                    live: false
                    function formatValue(v) {
                        return v.toFixed(2)
                    }
                    minimumValue: 0.00
                    maximumValue: 1.00
                    value: ImageProcessing.colorBias

                    width: biasRow.sliderWidth
                    height: units.gu(2)

                    onPressedChanged: {
                        if (!pressed) {
                            reprocessImage()
                        }
                    }
                }
            }

            Button {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Restore to default")
                color: UbuntuColors.green
                onClicked: {
                    colorSwitch.checked = false
                    filterSwitch.checked = true
                    colorThrSlider.value = 0.55
                    colorGainSlider.value = 1.5
                    colorBiasSlider.value = 0.0
                    reprocessImage()
                }
            }
        }
    }

    Connections {
        target: singleImageHeader

        onDeleteImage: {
            ImageProcessing.removeImage(currentID)
            pageStack.pop()
        }

        onSaveImage: {
            //TODO: move this code to a general function
            //TODO: save as pdf or as an image?
            var url = ImageProcessing.exportAsPdf(currentID)
            console.log("Share:", url)
            var sharePopup = PopupUtils.open(shareDialog, singleImagePage, {
                                                 "contentType": ContentType.Documents
                                             })
            sharePopup.items.push(contentItemComponent.createObject(
                                      singleImagePage, {
                                          "url": url,
                                          "text": currentID
                                      }))
        }
    }

    Component {
        id: shareDialog

        ContentShareDialog {
            Component.onDestruction: exportCompleted()
        }
    }

    Component {
        id: contentItemComponent
        ContentItem {}
    }

    Component.onDestruction: {
        currentImage = ""
        //TODO: Find a more elegant way of reseting this int
        currentIndex = -1
        currentID = ""
    }

    Component.onCompleted: {
        pageLoaded = true
    }

    Connections {
        target: ImageProcessing
        onImageUpdated: currentImage = "image://documents/" + id
    }

    function reprocessImage() {
        currentImage = ""
        ImageProcessing.reprocessImage(currentID, colorSwitch.checked,
                                       filterSwitch.checked,
                                       colorThrSlider.value,
                                       colorGainSlider.value,
                                       colorBiasSlider.value)
    }
}
