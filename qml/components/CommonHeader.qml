import QtQuick 2.6
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import ImageProcessing 1.0
import Ubuntu.Components.Popups 1.3

PageHeader {
    title: i18n.tr('Camera Scanner')

    StyleHints {
        foregroundColor: fgColor
        backgroundColor: bgColor
        dividerColor.visible: false
    }

    trailingActionBar {
        numberOfSlots: 4
        actions: [

            Action {
                iconName: "info"
                shortcut: "Ctrl+i"
                text: i18n.tr("Information")

                onTriggered: {
                    pageStack.push(Qt.resolvedUrl("../pages/InfoPage.qml"))
                }
            },

            Action {
                iconName: "add"
                shortcut: "Ctrl+a"
                text: i18n.tr("Add")

                onTriggered: {
                    Qt.inputMethod.hide()
                    pageStack.push(Qt.resolvedUrl("../pages/ImportPage.qml"), {
                                       "contentType": ContentType.Pictures,
                                       "handler": ContentHandler.Source
                                   })
                }
            },

            Action {
                iconName: "delete"
                shortcut: "Ctrl+Del"
                visible: imageModel.count != 0
                text: i18n.tr("Clear session")

                onTriggered: {
                    PopupUtils.open(clearSessionDialog, mainPage, {
                                        "confirmationDialogText": i18n.tr(
                                                                      "Clear session"),
                                        "descriptionLabelText": i18n.tr(
                                                                    "Do you really want to remove all images?"),
                                        "cancelButtonText": i18n.tr("Cancel"),
                                        "confirmButtonText": i18n.tr("Delete"),
                                        "confirmIsDestructive": true
                                    })
                }
            },

            Action {
                iconName: "save"
                shortcut: "Ctrl+s"
                visible: ImageProcessing.isAnyImage
                text: i18n.tr("Save")

                onTriggered: {
                    var url = ImageProcessing.exportAllAsPdf()
                    if (url) {
                        console.log("Share: " + url)
                        var sharePopup = PopupUtils.open(shareDialog,
                                                         mainPage, {
                                                             "contentType": ContentType.Documents
                                                         })
                        sharePopup.items.push(contentItemComponent.createObject(
                                                  mainPage, {
                                                      "url": url,
                                                      "text": "export"
                                                  }))
                    } else {
                        console.log("Sharing docs failed")
                        // TODO display proper message
                    }
                }
            }
        ]
    }
}
