#ifndef ONSEXTRACTOR_H
#define ONSEXTRACTOR_H

#include <opencv2/opencv.hpp>

namespace DocumentScanner {

class ONSExtractorConfig
{
public:
    bool colorMode = false;
    bool filterMode = true;
    int colorThr = 205;
    float colorGain = 1.5; // contrast
    float colorBias = 0; // brightness
};

/**
 * The OpenNoteScanner extractor is a reimplementation of the document
 * extraction algorithm used in the app OpenNoteScanner by
 * Claudemir Todo Bom (https://github.com/ctodobom/OpenNoteScanner).
 */
class ONSExtractor
{
public:
    bool extractDocument(const cv::Mat &rawImg, cv::Mat &docImg,
                         const ONSExtractorConfig conf = ONSExtractorConfig()) const;
};
} // namespace DocumentScanner

#endif
