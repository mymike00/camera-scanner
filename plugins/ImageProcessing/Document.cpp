#include "Document.h"

using namespace DocumentScanner;
using namespace cv;

Document::Document(const Mat &rawImg,
                   const ONSExtractor &extractor)
{
    rawImg.copyTo(m_rawImg);
    m_docImg = Mat();
    m_docExtracted = extractor.extractDocument(m_rawImg, m_docImg);
}

Document::Document(const Mat &rawImg, const Mat &docImg,
                   bool docExtracted /*= true*/)
    : m_docExtracted(docExtracted)
{
    rawImg.copyTo(m_rawImg);
    if (docExtracted)
        docImg.copyTo(m_docImg);
    else
        m_docImg = Mat();
}

void Document::reprocessImage(const ONSExtractor &extractor, const ONSExtractorConfig &conf)
{
    m_docExtracted = extractor.extractDocument(m_rawImg, m_docImg, conf);
}

Mat Document::getRawImage() const
{
    return m_rawImg;
}

Mat Document::getDocImage() const
{
    return m_docExtracted ? m_docImg : m_rawImg;
}

bool Document::docExtracted() const
{
    return m_docExtracted;
}
