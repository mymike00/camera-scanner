#include "ExtractorConfig.h"

using namespace DocumentScanner;

ExtractorConfig::ExtractorConfig(QObject *parent) : QObject(parent)
{
  m_db = new QSqlDatabase();
  createDataDir();
}

ExtractorConfig::~ExtractorConfig()
{
    closeDB();
}

void ExtractorConfig::createDataDir()
{
  QString pathDatabase = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + QDir::separator() + "database";

  QDir dir;
  if( !dir.mkpath(pathDatabase)) {
    qDebug() << "Error creating the database directory";
  } else {
    createDB(pathDatabase);
  }
}

void ExtractorConfig::createDB(QString path)
{
    QString url = path + "/ImageProcessingSettings.sqlite";

    *m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db->setDatabaseName(url);

    if (!m_db->open())
      qDebug() << "Error opening DB: " << m_db->lastError().text();

    createTableSettings();
}

void ExtractorConfig::createTableSettings()
{
    QSqlQuery query(*m_db);
    if( !query.exec("create table if not exists settings "
              "(id varchar(20), "
              "filterMode integer, "
              "colorMode integer, "
              "colorThr integer, "
              "colorGain real, "
              "colorBias real)") ) {
      qDebug() << "exec :" << query.lastError();
    }
}

void ExtractorConfig::addImageConfig(const QString& id)
{
    //Store default settings for new image
    ONSExtractorConfig conf;
    qDebug() << "add new image " << id << " with following values:" << conf.colorMode << conf.filterMode << conf.colorThr << conf.colorGain << conf.colorBias;
    QSqlQuery query(*m_db);
    //Store settings
    query.prepare("INSERT INTO settings (id, filterMode, colorMode, colorThr, colorGain, colorBias) "
                    "VALUES (:id, :filterMode, :colorMode, :colorThr, :colorGain, :colorBias)");
    query.bindValue(":id", id);
    query.bindValue(":filterMode", QVariant(conf.filterMode).toInt());
    query.bindValue(":colorMode", QVariant(conf.colorMode).toInt());
    query.bindValue(":colorThr", conf.colorThr);
    query.bindValue(":colorGain", conf.colorGain);
    query.bindValue(":colorBias", conf.colorBias);
    if( !query.exec())
        qDebug() << "exec :" << query.lastError();
}

void ExtractorConfig::editImageConfig(const QString& id, const ONSExtractorConfig& conf)
{
    qDebug() << "edit image " << id << " with following values:" << conf.colorMode << conf.filterMode << conf.colorThr << conf.colorGain << conf.colorBias;
    QSqlQuery query(*m_db);
    query.prepare("UPDATE settings SET filterMode = :filterMode, colorMode = :colorMode, colorThr = :colorThr, colorGain = :colorGain, colorBias = :colorBias WHERE id = :id");
    query.bindValue(":id", id);
    query.bindValue(":filterMode", conf.filterMode);
    query.bindValue(":colorMode", conf.colorMode);
    query.bindValue(":colorThr", conf.colorThr);
    query.bindValue(":colorGain", conf.colorGain);
    query.bindValue(":colorBias", conf.colorBias);
    if (!query.exec())
        qDebug() << "exec :" << query.lastError();
}

ONSExtractorConfig ExtractorConfig::loadImageConfig(const QString& id)
{
    ONSExtractorConfig conf;
    QSqlQuery query(*m_db);
    query.prepare("SELECT * FROM settings WHERE id = (:id)");
    query.bindValue(":id", id);

    if (!query.exec()) {
        qDebug() << "exec :" << query.lastError();
        return conf;
    } else {
        query.first();

        conf.filterMode = QVariant(query.value(1).toInt()).toBool();
        conf.colorMode = QVariant(query.value(2).toString()).toBool();
        conf.colorThr = query.value(3).toInt();
        conf.colorGain = query.value(4).toReal();
        conf.colorBias = query.value(5).toReal();
        qDebug() << "load image settings" << id << " with following values:" << conf.colorMode << conf.filterMode << conf.colorThr << conf.colorGain << conf.colorBias;
        return conf;
    }
}


void ExtractorConfig::removeImageConfig(const QString& id)
{
  QSqlQuery query(*m_db);
  query.prepare("DELETE FROM settings WHERE id = :id");
  query.bindValue(":id", id);
  if (!query.exec())
    qDebug() << "exec :" << query.lastError();
}

void ExtractorConfig::closeDB()
{
  qDebug() << "Close database";
  m_db->close();
}
